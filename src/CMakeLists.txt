cmake_minimum_required(VERSION 3.11)
project(Antlr4Runtime CXX)

if(CMAKE_SYSTEM_NAME MATCHES "Linux")
    find_package(PkgConfig REQUIRED)
    pkg_check_modules(UUID REQUIRED uuid)
endif()
if(APPLE)
    find_library(COREFOUNDATION_LIBRARY CoreFoundation)
endif()

set(ANTLR4_RUNTIME_BASE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/antlr4-cpp-runtime-4.7.2-source)
set(ANTLR4_RUNTIME_SOURCE_DIR ${ANTLR4_RUNTIME_BASE_DIR}/runtime)

file(GLOB libantlrcpp_SRC
    "${ANTLR4_RUNTIME_SOURCE_DIR}/src/*.cpp"
    "${ANTLR4_RUNTIME_SOURCE_DIR}/src/atn/*.cpp"
    "${ANTLR4_RUNTIME_SOURCE_DIR}/src/dfa/*.cpp"
    "${ANTLR4_RUNTIME_SOURCE_DIR}/src/misc/*.cpp"
    "${ANTLR4_RUNTIME_SOURCE_DIR}/src/support/*.cpp"
    "${ANTLR4_RUNTIME_SOURCE_DIR}/src/tree/*.cpp"
    "${ANTLR4_RUNTIME_SOURCE_DIR}/src/tree/pattern/*.cpp"
    "${ANTLR4_RUNTIME_SOURCE_DIR}/src/tree/xpath/*.cpp"
    )

if(MSVC)
  set_property(SOURCE "${ANTLR4_RUNTIME_SOURCE_DIR}/src/atn/ParserATNSimulator.cpp"
	PROPERTY COMPILE_DEFINITIONS _CRT_SECURE_NO_WARNINGS)
endif()

add_library(Antlr4Runtime STATIC ${libantlrcpp_SRC} Antlr4RuntimeConfig.cmake)
add_library(Antlr4Runtime::Antlr4Runtime ALIAS Antlr4Runtime)
## add include directories means of the following function
function(Antlr4RuntimAddIncludeDirectory includeDirectory)
    target_include_directories(Antlr4Runtime PUBLIC
        $<BUILD_INTERFACE:${ANTLR4_RUNTIME_SOURCE_DIR}/src/${includeDirectory}>
        $<INSTALL_INTERFACE:include/antlr4-runtime/${includeDirectory}>)
endfunction()

Antlr4RuntimAddIncludeDirectory(.)
Antlr4RuntimAddIncludeDirectory(atn)
Antlr4RuntimAddIncludeDirectory(misc)
Antlr4RuntimAddIncludeDirectory(support)
Antlr4RuntimAddIncludeDirectory(tree)
Antlr4RuntimAddIncludeDirectory(tree/pattern)
Antlr4RuntimAddIncludeDirectory(tree/xpath)
target_include_directories(Antlr4Runtime INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>)

set_target_properties(Antlr4Runtime PROPERTIES CXX_STANDARD 11 CXX_STANDARD_REQUIRED ON)


if(CMAKE_SYSTEM_NAME MATCHES "Linux")
    target_link_libraries(Antlr4Runtime PUBLIC ${UUID_LIBRARIES})
elseif(APPLE)
    target_link_libraries(Antlr4Runtime PUBLIC ${COREFOUNDATION_LIBRARY})
endif()

target_compile_definitions(Antlr4Runtime PUBLIC ANTLR4CPP_STATIC=1)

if (CMAKE_CXX_COMPILER_ID MATCHES "MSVC")
    set(disabled_compile_warnings "/wd4251")
else()
    set(disabled_compile_warnings "-Wno-overloaded-virtual")
endif ()

if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
    set(disabled_compile_warnings "${disabled_compile_warnings} -Wno-dollar-in-identifier-extension -Wno-four-char-constants")
    set(disabled_compile_warnings "${disabled_compile_warnings} -Wno-defaulted-function-deleted")
elseif("${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU" OR "${CMAKE_CXX_COMPILER_ID}" MATCHES "Intel")
    set(disabled_compile_warnings "${disabled_compile_warnings} -Wno-multichar -Wno-attributes")
endif()


set_target_properties(Antlr4Runtime
    PROPERTIES VERSION   ${ANTLR_VERSION}
    SOVERSION ${ANTLR_VERSION}
    COMPILE_FLAGS ${disabled_compile_warnings} )

install(TARGETS Antlr4Runtime
    EXPORT Antlr4RuntimeTargets
    LIBRARY DESTINATION lib COMPONENT Antlr4Runtime
    ARCHIVE DESTINATION lib COMPONENT Antlr4Runtime
    RUNTIME DESTINATION bin COMPONENT Antlr4Runtime
    INCLUDES DESTINATION include)

install(EXPORT Antlr4RuntimeTargets
    FILE Antlr4RuntimeTargets.cmake
    NAMESPACE Antlr4Runtime::
    DESTINATION lib/cmake/Antlr4Runtime 
	COMPONENT Antlr4Runtime)

install(DIRECTORY "${ANTLR4_RUNTIME_SOURCE_DIR}/src/"
    DESTINATION "include/antlr4-runtime"
    COMPONENT Antlr4Runtime
    FILES_MATCHING PATTERN "*.h"
    )
    
install(FILES antlr4-runtime-wrapper.h DESTINATION "include/antlr4-runtime" COMPONENT Antlr4Runtime)    


include(CMakePackageConfigHelpers)
write_basic_package_version_file(
    "Antlr4RuntimeConfigVersion.cmake"
    VERSION 4.7.2
    COMPATIBILITY ExactVersion)

install(FILES "Antlr4RuntimeConfig.cmake"
    "${CMAKE_CURRENT_BINARY_DIR}/Antlr4RuntimeConfigVersion.cmake"
    DESTINATION lib/cmake/Antlr4Runtime  COMPONENT Antlr4Runtime)

install(FILES ${ANTLR4_RUNTIME_BASE_DIR}/LICENSE.txt
        ${ANTLR4_RUNTIME_BASE_DIR}/README.md 
        ${ANTLR4_RUNTIME_BASE_DIR}/VERSION
        DESTINATION doc/Antlr4Runtime  COMPONENT Antlr4Runtime)

### CPACK stuff ###
set(CPACK_PACKAGE_NAME "Antlr4Runtime")
SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "C++ Runtime for ANTLR4")
SET(CPACK_PACKAGE_VENDOR "raha01")
SET(CPACK_PACKAGE_VERSION_MAJOR "4")
SET(CPACK_PACKAGE_VERSION_MINOR "7")
SET(CPACK_PACKAGE_VERSION_PATCH "2")
#SET(CPACK_IFW_VERBOSE ON)
include(CPack)
include(CPackIFW)
cpack_add_component(Antlr4Runtime DISPLAY_NAME "Antlr4Runtime" DESCRIPITON "Antlr4Runtime")
cpack_ifw_configure_component(Antlr4Runtime
    VERSION "4.7.2" # Version of component
    DEFAULT TRUE)
