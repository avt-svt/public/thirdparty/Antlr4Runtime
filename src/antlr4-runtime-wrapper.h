#ifndef ANTLRWRAPPER_HPP
#define ANTLRWRAPPER_HPP


#if (defined __GNUC__ && !defined __clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wattributes"
#include "antlr4-runtime.h"
#pragma GCC diagnostic pop

#elif (defined _MSC_BUILD && !defined __clang__)
#pragma warning(push) 
#pragma warning(disable : 4996)
#pragma warning(disable : 26495)
#pragma warning(disable : 26439)
#include "antlr4-runtime.h"
#pragma warning(pop)

#elif defined __clang__
#pragma clang diagnostic push
#if __clang_major__ >= 9
#pragma clang diagnostic ignored "-Wdefaulted-function-deleted"
#endif
#pragma clang diagnostic ignored "-Wattributes"
#include "antlr4-runtime.h"
#pragma clang diagnostic pop

#else
#include "antlr4-runtime.h"
#endif

#ifndef __has_cpp_attribute         // For backwards compatibility
#define __has_cpp_attribute(x) 0
#endif


#endif // ANTLRWRAPPER_HPP
